<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Models\Plane;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging());
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});


//--------------------------------CHECK-------------------------------------------
$app->get('/customers/{cnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber']);
});

$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber' ]. ' Product '.$args['pnumber' ]);
});



//--------------------------------MESSAGES-------------------------------------------
$app->get('/messages', function($request, $response,$args){
   $_message = new Message();
   $messages = $_message->all();
   $payload = [];
   foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at,
            'updated_at'=>$msg->updated_at
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->get('/messages/{id}', function($request, $response,$args){
$_id = $args['id'];
$message = Message::find($_id);

   return $response->withStatus(200)->withJson($message);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid = $request->getParsedBodyParam('userid','');
   $_message = new Message();
   $_message->body = $message;
   $_message->user_id = $userid;
   $_message->save();
   if($_message->id){
       $payload = ['message_id'=>$_message->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/messages/{message_id}', function($request, $response,$args){
    $_message = Message::find($args['message_id']);
    $_message->delete();
    if($_message->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $_message = Message::find($args['message_id']);
    //die("message id is " . $_message->id);
    $_message->body = $message;
    if($_message->save()){
        $payload = ['message_id'=>$_message->id,"result"=>"The message has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
         return $response->withStatus(400);
    }
});

//--------------------------------MESSAGES BULK-------------------------------------------
$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    Message::insert($payload);
    return $response->withStatus(200)->withJson($payload);

});

$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();//�� ���� ���� �� �� �'����� ������� ����� �� ������� ����� 
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

$app->delete('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();//�� ���� ���� �� �� �'����� ������� ����� �� ������� ����� 

    User::trash($payload);
    return $response->withStatus(201)->withJson($payload);
});


//--------------------------------USERS-------------------------------------------
$app->get('/users', function($request, $response,$args){
   $_user = new User();
   $users = $_user->all();
   $payload = [];
   foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=>$usr->id,
            'username'=>$usr->username,
            'email'=>$usr->email
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->get('/users/{id}', function($request, $response,$args){
$_id = $args['id'];
$user = User::find($_id);

   return $response->withStatus(200)->withJson($user);
});

$app->post('/users', function($request, $response,$args){
    $username = $request->getParsedBodyParam('username','');
    $email = $request->getParsedBodyParam('email','');
  //  $password = $request->getParsedBodyParam('password','');
   $_user = new User();
   $_user->username = $username;
   $_user->email = $email;
   //$_user->password = $password;
   $_user->save();
   if($_user->id){
       $payload = ['user id: '=>$_user->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/users/{id}', function($request, $response,$args){
    $user = User::find($args['id']);
    $user->delete();
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->put('/users/{user_id}', function($request, $response,$args){
    $username = $request->getParsedBodyParam('username','');
    $email = $request->getParsedBodyParam('email','');;
    $_user = User::find($args['user_id']);
    $_user->username = $username;
    $_user->email = $email;

    if($_user->save()){ //�� ���� ����
        $payload = ['user_id' => $_user->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

//--------------------------------PLANES-------------------------------------------

$app->get('/planes', function($request, $response,$args){
   $_plane = new Plane();
   $planes = $_plane->all();
   $payload = [];
   foreach($planes as $prd){
        $payload[$prd->id] = [
            'id'=>$prd->id,
            'manufacturer'=>$prd->manufacturer,
            'model'=>$prd->model
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->get('/planes/{id}', function($request, $response,$args){
$_id = $args['id'];
$plane = Plane::find($_id);

   return $response->withStatus(200)->withJson($plane);
});

$app->put('/planes/{id}', function($request, $response,$args){
    $manufacturer = $request->getParsedBodyParam('manufacturer','');
    $model = $request->getParsedBodyParam('model','');;
    $_plane = Plane::find($args['id']);
    $_plane->manufacturer = $manufacturer;
    $_plane->model = $model;

    if($_plane->save()){ //�� ���� ����
        $payload = ['id' => $_plane->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->post('/planes/search', function($request, $response, $args){    
    $manufacturer= $request->getParsedBodyParam('manufacturer',''); 
    $planes  = Plane::where('manufacturer', '=', $manufacturer)->get(); 
    $payload=[];
    foreach($products as $prd){
        $payload[$prd->id] = [
           'id'=>$prd->id,
            'manufacturer'=>$prd->manufacturer,
            'model'=>$prd->model
        ];
    }

    return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
});

$app->delete('/planes/{id}', function($request, $response,$args){
    $plane = Plane::find($args['id']);
    $plane->delete();
    if($plane->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});


//--------------------------------JWT-------------------------------------------
/// JWT - ���� �� �� ����� �����,����� ����� ��� ���� ������ ������ ������� �� ��'����� ������
/*
$app->post('/auth', function($request, $response,$args){
    $user = $request->getParsedBodyParam('user',''); //����� ���� �� �� ������ �������� ������� �����
    $password = $request->getParsedBodyParam('password',''); // ����� ���� �� �����
    // we need to check DB, for not hard coded
    if($user == 'jack' && $password == '1234' ){// ����� �����
    //we need to generate JWT but no now.
        //create JWT and send to client
        $payload = ['token'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3IiwibmFtZSI6ImphY2siLCJhZG1pbiI6dHJ1ZX0.PETyPBssJTN1V8fSwPxkE0Hftg3hGYux4Rx1WcMC5NA'];
        return $response->withStatus(200)->withJson($payload);
    } 
    else{
        $payload = ['token'=>null];
          return $response->withStatus(403)->withJson($payload);
    }

});

//$app->add(new \Slim\Middleware\JwtAuthentication([ // ���� ������ ���� ����� ���� ��'������ - ����� �����
//    "secret" => "supersecret",
 //   "path"=> ['/messages']// ��� ������� ������ ����� ��������� ���� �����
//]));

*/

//--------------------------------LOGIN WITHOUT JWT-------------------------------------------

$app->post('/login', function($request, $response,$args){
    $username  = $request->getParsedBodyParam('username','');
    $password = $request->getParsedBodyParam('password','');    
    $_user = User::where('username', '=', $username)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
});


$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});


$app->run();